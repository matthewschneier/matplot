import pygal 

from die import Die

print(
    'Roll as mnay dice as you want as many times as you want and return graph of product.')
#Create as many dice as user wants.
num_dice = 0
user_sides = input('What sided die would you like to roll? (non-number to stop) ')
user_choices = []
while True:
    try:
        user_sides = int(user_sides)
    except ValueError:
        break
    else:
        user_choices.append(user_sides)
        num_dice += 1
        user_sides = input('Another die? (non-number to stop) ')

#Roll dice and store results.
results = []
num_rolls = input('How many rolls? ')
while True:
    try:
        num_rolls = int(num_rolls) * 2
    except ValueError:
        num_rolls = input('Roll a number.')
    else:
        values = []
        value = 1
        for roll in range(num_rolls):
            for choice in user_choices:
                die = Die(choice)
                value *= die.roll()
            values.append(value)
        for value in values:
            num = values.pop(-1)
            num /= values[-1]
            results.append(num)
        break

#Analyze the results.
frequencies = []
max_roll = 1
for choice in user_choices:
    max_roll *= choice
for side in range(num_dice, max_roll + 1):
    freq = results.count(side)
    frequencies.append(freq)

#Visualize the results.
hist = pygal.Bar()
num_rolls //= 2
title = 'Resutl of rolling '+str(num_dice)+' different-sided dice '+str(num_rolls)+' times.'
hist.title = title
#Make x labels
hist.x_labels = []
for label in range(num_dice,max_roll + 1):
    hist.x_labels.append(str(label))
hist.x_title = 'Result'
hist.y_title = 'Frequency'
#Make hist.add look right.
legend = ''
for num in user_choices:
    legend += str(num) + ' '
hist.add(legend + 'sided dice', frequencies)
filename = input('Filename? ')
hist.render_to_file(filename + '.svg')
